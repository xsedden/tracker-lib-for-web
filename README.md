#Task Tracker
##What can this application use?

* $ Lab2 create_person    > *Create user*
* $ Lab2 addperson        > *Add user*
* $ Lab2 showperson       > *Show users*
* $ Lab2 removeperson     > *Remove user*
* $ Lab2 loginperson      > *Login user*
* $ Lab2 exitperson       > *Exit user*
* $ Lab2 task             > *Create tasks*
* $ Lab2 show             > *Demonstrating all the tasks that user has*
* $ Lab2 edit             > *Edit and remove tasks*
* $ Lab2 recurrent        > *Create recurrent tasks*
* $ Lab2 rrec             > *Remove recurrent tasks*
* $ Lab2 show_rec         > *Demonstrating all the recurrent tasks that user has*
* $ Lab2 setconfig        > *Set path to config*
* $ Lab2 pathconfig       > *show path to config*



##How to install ?

    $ python setup.py install
    
    
##How to start tests ?

    $ python setup.py test


Made by Mikhail Shchasnovich.

Group 653502.