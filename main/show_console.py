from main.lib.person import check_login
from main.lib.show import show_active, show_all
from main.lib.convert_data import get_rec_ids,get_rec_name_by_id,get_rec_type_by_id,get_rec_nextdate_by_id, get_login_person, get_persons, get_status_by_id, get_name_by_id, get_createdate_by_id, get_ids_parents_by_id, get_priority_by_id, get_deadline_by_id
from main.lib.person import check_show

def show_reccurent():
    check_login()
    for rec_id in get_rec_ids():
        print('{0:2} | {1:20} | {2:10} | {3:10}'.format(rec_id, get_rec_name_by_id(rec_id), get_rec_type_by_id(rec_id),
                                                        get_rec_nextdate_by_id(rec_id)))


def show_login_person():
    print(get_login_person())


def show_persons():
    for person in get_persons():
        print(person)


def show_task(args):
    for task_id in show_active(args):
        full_show(task_id)


def show_task_all(args):
    for task_id in show_all(args):
        full_show(task_id)


def full_show(task_id):
    if get_status_by_id(task_id) is None:
        print('{0:3} | {1:50} | {2:8} | {3:8} | {4:3}'.format(task_id, _spases_tree(task_id) + get_name_by_id(task_id),
                                                                  convert_date(get_createdate_by_id(task_id)),
                                                                  get_deadline_by_id(task_id),
                                                                  get_priority_by_id(task_id)))
    if get_status_by_id(task_id) == 1:
        print('{0:3} | \033[92m{1:50}\033[00m | {2:8} | {3:8} | {4:3}'.format(task_id,
                                                                                  _spases_tree(task_id) + get_name_by_id(
                                                                                      task_id),
                                                                                  convert_date(
                                                                                      get_createdate_by_id(task_id)),
                                                                                  get_deadline_by_id(task_id),
                                                                                  get_priority_by_id(task_id)))
    if get_status_by_id(task_id) == 2:
        print('{0:3} | \033[91m{1:50}\033[00m | {2:8} | {3:8} | {4:3}'.format(task_id,
                                                                                  _spases_tree(task_id) + get_name_by_id(
                                                                                      task_id),
                                                                                  convert_date(
                                                                                      get_createdate_by_id(task_id)),
                                                                                  get_deadline_by_id(task_id),
                                                                                  get_priority_by_id(task_id)))


def _spases_tree(task_id):
    spase = ''
    for task in get_ids_parents_by_id(task_id):
        if check_show(task):
            spase += ' '
    return spase


def convert_date(date):
    return date.replace(".", "")[:8]
