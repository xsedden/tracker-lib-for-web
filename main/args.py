import argparse
import os
from datetime import datetime

from main.lib.task import Task
from main.lib.person import Person
from main.lib import exceptions
from main.lib.config import take_dir_path, load_config
from main.lib.convert_data import add_task_to_file
from main.lib.edit import finish_task, delete_task, edit_text, edit_deadline, edit_famaly, edit_name, edit_priority
from main.lib.exceptions import eprint
from main.lib.person import add_access_person, remove_access_person, add_access_admin, remove_access_admin
from main.lib.person import login_person, exit_person
from main.lib.person import get_login_person
from main.lib.recurrent_task import Recurrent, remove_rec
from main.lib.show import show_all, show_text, show_active
from main.show_console import show_reccurent, show_login_person, show_persons, show_task, show_task_all


def show(args):
    if args.showall:
        show_task_all(args)
    if args.showtext:
        show_text(args.showtext)
    if not (args.showtext or args.showall):
        show_task(args)


def task(args):
    task = Task(args.name, args.text, args.parent, args.priority, args.deadline, args.creater)
    add_task_to_file(task)


def edit(args):
    if args.task_id:
        if args.editstatus:
            finish_task(args.task_id)
        if args.delete:
            delete_task(args.task_id)
        if args.text:
            edit_text(args.task_id)
        if args.editdeadline:
            edit_deadline(args.task_id, args.editdeadline)
        if args.editfamaly:
            edit_famaly(args.task_id, args.editfamaly)
        if args.name:
            edit_name(args.task_id, args.name)
        if args.priority:
            edit_priority(args.task_id, args.priority)


def login(args):
    if args.login and args.password:
        login_person(args.login, args.password)


def exit_per(args):
    exit_person()


def create_persons(args):
    if args.login and args.password:
        pers = Person(args.login, args.password)
        pers.create_person()
    else:
        raise exceptions.UserError(' Для создания пользователя необходимо ввести логин и пароль.')


def show_person_args(args):
    if args.logperson:
        show_login_person()
    if args.allpersons:
        show_persons()


def remove_person(args):
    if args.task_id and args.person:
        remove_access_person(args.login, args.task_id)
    if args.task_id and args.admin:
        remove_access_admin(args.login, args.task_id)


def add_person(args):
    if args.task_id and args.person and args.login:
            add_access_person(args.login, args.task_id)
    elif args.task_id and args.admin and args.login:
            add_access_admin(args.login, args.task_id)
    else:
        raise exceptions.UserError('Необходимо больше аргументов.(id задания, имя добавляемого пользователя, права доступа)')


def pathconfig(args):
    print(take_dir_path())


def reccurent(args):
    rec = Recurrent(args.typetask, args.text, args.parent, args.name, args.start_date)
    rec.add_rec()


def remove_reccurent(args):
    if args.rec_id:
        remove_rec(args.rec_id)


def show_rec(args):
    show_reccurent()


parser = argparse.ArgumentParser()
parser.add_argument('-q', dest="silence", action="store_true", help='silence mode(without print logs to console).')
subparsers = parser.add_subparsers()

task_parser = subparsers.add_parser('task', help='Add task')
task_parser.add_argument('-n', nargs='?', dest="name", type=str, default="name", help='set name for task')
task_parser.add_argument('-t', nargs='?', dest="text", type=str, default="default_text.txt", help='set path for text')
task_parser.add_argument('-f', nargs='?', dest="parent", type=int, default=None, help='set parent for task')
task_parser.add_argument('-p', nargs='?', dest="priority", type=int, default=10, help='set priority for task')
task_parser.add_argument('-d', nargs='?', dest="deadline", type=int, default=99999999, help='YYYYMMDD set deadline for task')
task_parser.add_argument('-c', nargs='?', dest="creater", type=str, default=get_login_person(), help='set creater for task')
task_parser.set_defaults(func=task)

show_parser = subparsers.add_parser('show', help='show tasks')
show_parser.add_argument('-a', dest="showall", action="store_true", help='show all task')
show_parser.add_argument('-i', nargs=2, dest="showbyinterval", help='YYYYMMDD and YYYYMMDD show tasks in interval create date')
show_parser.add_argument('-f', nargs=2, dest="showbyintervaldeadline", help='YYYYMMDD and YYYYMMDD show tasks in interval deadline')
show_parser.add_argument('-p', dest="showbypriority", action="store_true", help='with sort by priority')
show_parser.add_argument('-d', dest="showbydeadline", action="store_true", help='with sort by deadline')
show_parser.add_argument('-c', dest="showbycreatedate", action="store_true", help='with sort by create date')
show_parser.add_argument('-t', dest="showtext", type=int, help='write id task for show text')
show_parser.set_defaults(func=show)

edit_parser = subparsers.add_parser('edit', help='edit tasks')
edit_parser.add_argument('-s', dest="editstatus", action="store_true", help='switch status for task')
edit_parser.add_argument('-n', nargs='?', dest="name", type=str, help='edit name for task')
edit_parser.add_argument('-d', dest="delete", action="store_true", help='remove task')
edit_parser.add_argument('-l', nargs='?', dest="editdeadline", type=int, help='YYYYMMDD edit deadline for task')
edit_parser.add_argument('-i', dest="task_id", type=int, help='id task for edit')
edit_parser.add_argument('-p', nargs='?', dest="priority", type=int, help='edit priority for task')
edit_parser.add_argument('-f', nargs='?', dest="editfamaly", type=int, help='edit parent for task')
edit_parser.add_argument('-t', dest="text", action="store_true", help='edit text for task')
edit_parser.set_defaults(func=edit)

login_parser = subparsers.add_parser('login', help='login person')
login_parser.add_argument('-l', nargs='?', dest="login", type=str, help='write your login')
login_parser.add_argument('-p', nargs='?', dest="password", type=str, help='write your password')
login_parser.set_defaults(func=login)

exit_parser = subparsers.add_parser('exit', help='exit person')
exit_parser.set_defaults(func=exit_per)

create_person_parser = subparsers.add_parser('create_person', help='create person')
create_person_parser.add_argument('-l', nargs='?', dest="login", type=str, help='set new login')
create_person_parser.add_argument('-p', nargs='?', dest="password", type=str, help='set password')
create_person_parser.set_defaults(func=create_persons)

showperson_parser = subparsers.add_parser('showperson', help='show person')
showperson_parser.add_argument('-l', dest="logperson", action="store_true", help='show person in system')
showperson_parser.add_argument('-p', dest="allpersons", action="store_true", help='show all persons')
showperson_parser.set_defaults(func=show_person_args)

addperson_parser = subparsers.add_parser('addperson', help='add person')
addperson_parser.add_argument('-i', nargs='?', dest="task_id", type=int, help='id task for edit')
addperson_parser.add_argument('-l', nargs='?', dest="login", type=str, help='login person for add')
addperson_parser.add_argument('-p', dest="person", action="store_true", help='set as person')
addperson_parser.add_argument('-a', dest="admin", action="store_true", help='set as admin')
addperson_parser.set_defaults(func=add_person)

removeperson_parser = subparsers.add_parser('removeperson', help='remove person')
removeperson_parser.add_argument('-i', nargs='?', dest="task_id", type=int, help='id task for edit')
removeperson_parser.add_argument('-l', nargs='?', dest="login", type=str, help='login person for remove')
removeperson_parser.add_argument('-p', dest="person", action="store_true", help='remove as person')
removeperson_parser.add_argument('-a', dest="admin", action="store_true", help='remove as admin')
removeperson_parser.set_defaults(func=remove_person)

setconfig_parser = subparsers.add_parser('setconfig', help='set config')
setconfig_parser.add_argument('-p', nargs='?', dest="set_config_path", type=str, help='new path for config')

pathconfig_parser = subparsers.add_parser('pathconfig', help='show config path')
pathconfig_parser.set_defaults(func=pathconfig)

reccurent_parser = subparsers.add_parser('recurrent', help='add recurrent task')
reccurent_parser.add_argument('-y', nargs='?', dest="typetask", choices=['d', 'w', 'm'], default='w', help='choose some from: d - day mode, w - week mode, m - month node.')
reccurent_parser.add_argument('-t', nargs='?', dest="text", type=str, default='defaulttext.txt', help='name text for recurrent task')
reccurent_parser.add_argument('-f', nargs='?', dest="famaly", type=int, default=None, help='parent for recurrent task')
reccurent_parser.add_argument('-n', nargs='?', dest="name", type=str, default='Name_rec', help='name for recurrent task')
reccurent_parser.add_argument('-s', nargs='?', dest="start_date", type=int, default=datetime.strftime(datetime.now(), "%Y%m%d"), help='YYYYMMDD date for start recurrent task')
reccurent_parser.set_defaults(func=reccurent)

show_reccurent_parser = subparsers.add_parser('show_rec', help='show recurrent tasks')
show_reccurent_parser.set_defaults(func=show_rec)

remove_reccurent_parser = subparsers.add_parser('rrec', help='remove reccurent')
remove_reccurent_parser.add_argument('-i', dest="rec_id", type=int, help='id recurrent task for remove')
remove_reccurent_parser.set_defaults(func=remove_reccurent)
