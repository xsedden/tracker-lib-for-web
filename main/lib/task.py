import subprocess
import os
from datetime import datetime
###from main.lib.config import take_tasks_path, take_text_path
###from main.lib.convert_data import take_last_id


class Task:

    def __init__(self, name, text, famaly, priority, deadline, creater):
        self.task_name = name
        self.task_text = text
        self.task_famaly = famaly
        self.task_priority = priority
        self.task_deadline = deadline
        self.task_creater = creater

        self.set_create_date()
        self.set_status(0)


    def set_create_date(self):
        self.create_date = datetime.now()


    def set_id_task(self):
        self.task_id = 1


    def set_status(self, status):
        self.task_status = status


