from __future__ import print_function
import sys


class Error(Exception):
    pass


class UserError(Error):
    def __init__(self, msg):
        self.msg = msg


class TaskError(Error):
    def __init__(self, msg):
        self.msg = msg


class IDError(Error):
    def __init__(self, msg):
        self.msg = msg


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
