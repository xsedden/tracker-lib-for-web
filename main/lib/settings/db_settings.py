from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine('postgresql+psycopg2://admin:admin@{}:5432/db'.format('default'))

Session = scoped_session(sessionmaker(bind=engine))


try:
    pass  # TODO import settings
except ImportError:
    pass
