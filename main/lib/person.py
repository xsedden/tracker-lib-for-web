import logging

from main.lib import exceptions
from main.lib.convert_data import set_login_password, get_login_person, check_log_pass, set_login_person, get_persons, \
    exit_person_f, get_admin_by_id, get_person_by_id, get_creater_by_id, set_person_by_id, get_ids_by_famaly, \
    set_admin_by_id, remove_admin_by_id, remove_person_by_id
###from main.lib.exceptions import eprint

class Person:

    def __init__(self, login, password):
        self.login = login
        self.password = password

    def create_person(self):
        set_login_password(self.login, self.password)
        logging.info("person {} was created".format(self.login))


def check_login():
    if get_login_person() is None:
        raise exceptions.UserError('Необходимо залогиниться.')


def login_person(login, password):
    if check_log_pass(login, password):
        set_login_person(login)
        logging.info("person {} was logined.".format(login))
    else:
        raise exceptions.UserError('Неверные имя пользователя или пароль.')


def exit_person():
    exit_person_f()


def check_access_admin(task_id, user):
    if user in get_admin_by_id(task_id) or check_access_creater(task_id, user):
        return True


def check_access_creater(task_id, user):
    if user == get_creater_by_id(task_id):
        return True


def check_access_person(task_id, user):
    if user in get_person_by_id(task_id) or check_access_admin(task_id, user) or check_access_creater(task_id, user):
        return True


def check_show(task_id, user):
    if user in get_creater_by_id(task_id): #### get_ person!!!
        return True


def add_access_person(login, task_id, user):
    if check_access_admin(task_id, user):
        set_person_by_id(task_id, login)
        if get_ids_by_famaly(task_id):
            _rec_add_access_person(login, task_id)
        #logging.info("person {0} was added to task {1}.".format(login, task_id))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def _rec_add_access_person(login, task_id):
    for task in get_ids_by_famaly(task_id):
        set_person_by_id(task, login)
        if get_ids_by_famaly(task):
            _rec_add_access_person(login, task)


def remove_access_person(login, task_id, user):
    if check_access_creater(task_id, user):
        remove_person_by_id(task_id, login)
        if get_ids_by_famaly(task_id):
            _rec_remove_access_person(login, task_id)
    #    logging.info("person {0} was removed from task {1}.".format(login, task_id))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')



def _rec_remove_access_person(login, task_id):
    for task in get_ids_by_famaly(task_id):
        remove_person_by_id(task, login)
        if get_ids_by_famaly(task_id):
            _rec_remove_access_person(login, task)


def add_access_admin(login, task_id, user):
    if check_access_creater(task_id, user):
        set_admin_by_id(task_id, login)
        set_person_by_id(task_id, login)
        if get_ids_by_famaly(task_id):
            _rec_add_access_admin(login, task_id)
    #    logging.info("admin {0} was added to task {1}.".format(login, task_id))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')



def _rec_add_access_admin(login, task_id):
    for task in get_ids_by_famaly(task_id):
        set_admin_by_id(task, login)
        set_person_by_id(task_id, login)
        if get_ids_by_famaly(task_id):
            _rec_add_access_admin(login, task)


def remove_access_admin(login, task_id, user):
    if check_access_creater(task_id, user):
        remove_admin_by_id(task_id, login)
        if get_ids_by_famaly(task_id):
            _rec_remove_access_admin(login, task_id)
    #    logging.info("admin {0} was removed from task {1}.".format(login, task_id))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def _rec_remove_access_admin(login, task_id):
    for task in get_ids_by_famaly(task_id):
        remove_admin_by_id(task, login)
        if get_ids_by_famaly(task_id):
            _rec_remove_access_admin(login, task)
