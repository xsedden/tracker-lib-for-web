from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, String, DateTime, Integer, ForeignKey

from main.lib.models.mixins import AbstractTimeStampMixin

Base = declarative_base()


class User(AbstractTimeStampMixin, Base):
    __tablename__ = "user"
    id = Column(Integer)