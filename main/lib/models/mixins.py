from sqlalchemy import DateTime, Column
from datetime import datetime

from sqlalchemy.orm import declarative_mixin


@declarative_mixin
class AbstractTimeStampMixin:
    created_at = Column(DateTime(), default=datetime.now)
    updated_at = Column(DateTime(), default=datetime.now, onupdate=datetime.now)