from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, String, DateTime, Integer, ForeignKey

from main.lib.models.mixins import AbstractTimeStampMixin

Base = declarative_base()


class Task(AbstractTimeStampMixin, Base):
    __tablename__ = 'task'
    id = Column(Integer, primary_key=True)
    description = Column(String())
    deadline = Column(DateTime())

    recurrent_type = Column()

    parent_id = Column(Integer(), ForeignKey('task.id'), nullable=True)
    parent = relationship("Task", backref="children")

    creator_id = Column(Integer(), ForeignKey('user.id'), nullable=False)
    creator = relationship("User", backref="tasks")

    priority_id = Column(Integer(), ForeignKey('priority.id'))
    priority = relationship("Priority", backref="tasks")

    status_id = Column(Integer(), ForeignKey('status.id'))
    status = relationship("Status", backref="tasks")
