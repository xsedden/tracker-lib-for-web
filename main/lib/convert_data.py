import json
import os
import logging
import subprocess
from datetime import datetime

from main.lib.config import take_login_path, take_users_path, take_recurrent_path
from main.lib.config import take_tasks_path, take_text_path
from main.lib.sort import sort_by_priority, sort_by_deadline, sort_by_create_date
from main.lib import exceptions
from peewee import *


#_database_proxy = Proxy()
db = SqliteDatabase(os.path.join(os.path.expanduser('~/.track'), 'db.sqlite3'))


class BaseTableClass(Model):

    class Meta:

        database = db


class TaskTable(BaseTableClass):

    id = AutoField(primary_key=True)
    text = TextField()
    name = CharField()
    create_date = DateField(default=datetime.now())
    deadline = DateField(default=datetime.now())
    creater = CharField()
    parent_id = IntegerField(default=0)
    priority = IntegerField(default=10)
    status = IntegerField()


    class Meta:

        db_table = 'task_task'


class AdminTable(BaseTableClass):
    task = ForeignKeyField(TaskTable,on_delete='CASCADE', related_name='admins')
    admins_name = CharField()

    class Meta:

        db_table = 'task_admin'


class PersonTable(BaseTableClass):
    task = ForeignKeyField(TaskTable,on_delete='CASCADE', related_name='persons')
    persons_name = CharField()

    class Meta:

        db_table = 'task_person'


class RecurrentTable(BaseTableClass):

    id = AutoField(primary_key=True)
    typetask = CharField(choices=['w', 'd', 'm'], default='w')
    name = CharField()
    create_date = DateField(default=datetime.now())
    startdate = DateField(default=datetime.now())
    creater = CharField()
    parent_id = IntegerField()
    text = TextField()
    priority = IntegerField(default=10)


    class Meta:

        db_table = 'recurrent_recurrent'

"""
class UserTable(BaseTasbleClass):
    login = CharField()
    password = CharField()
"""


###+++++++++
def add_task_to_file(task):
    #_database_proxy.initialize(db)
    table = TaskTable()
    if isinstance(task, dict):
        table.priority = task.get("priority")
        table.text = task.get("text")
        table.create_date = task.get("create_date")
        table.deadline = task.get("deadline")
        table.name = task.get("name")

        table.creater = task.get("creater")
        table.parent_id = task.get("parent_id")
        table.status = task.get("status")
    else:
        table.priority = task.task_priority
        table.text = task.task_text
        table.create_date = task.create_date
        table.deadline = task.task_deadline
        table.name = task.task_name

        table.creater = task.task_creater
        table.parent_id = task.task_famaly
        table.status = task.task_status

    table.save()

###---
def get_tasks_id_by_sort(args):
    jsondata = from_file()
    if args.showbypriority:
        jsondata.sort(key=sort_by_priority)
    if args.showbydeadline:
        jsondata.sort(key=sort_by_deadline)
    if args.showbycreatedate:
        jsondata.sort(key=sort_by_create_date)
    out_list = list()
    if jsondata:
        for dic in jsondata:
            out_list.append(dic.get("task_id"))
    return out_list

##++
def get_tasks_ids():
    out_list = list()
    for task in TaskTable.select():
        out_list.append(task.id)
    return out_list


def get_tasks():
    out_list = list()
    for task in TaskTable.select():
        out_list.append(task)
    return out_list


###+++
def get_ids_by_famaly(task_id):
    out_list = list()
    for task in TaskTable.select().where(TaskTable.parent_id == task_id):
        out_list.append(task.id)
    return out_list

###++
def get_text_by_id(task_id):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    return task.text

###++
def get_famaly_by_id(task_id):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    return task.parent_id


##+
def get_status_by_id(task_id):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    return task.status


##+
def get_createdate_by_id(task_id):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    return int(datetime.strftime(task.create_date, "%Y%m%d"))

##+
def get_deadline_by_id(task_id):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    return int(datetime.strftime(task.deadline, "%Y%m%d"))

##+++
def get_person_by_id(task_id):
    out_list = []
    if check_id(task_id):
        task = TaskTable.select().where(TaskTable.id == task_id).get()
        for person in task.persons:
            out_list.append(person.persons_name)
        return out_list
    else:
        return out_list

##+++
def get_admin_by_id(task_id):
    out_list = []
    if check_id(task_id):
        task = TaskTable.select().where(TaskTable.id == task_id).get()
        for admin in task.admins:
            out_list.append(admin.admins_name)
        return out_list
    else:
        return out_list

##++
def get_creater_by_id(task_id):
    if check_id(task_id):
        task = TaskTable.select().where(TaskTable.id == task_id).get()
        return task.creater
    else:
        return False

################################
#+
def set_name_by_id(task_id, name):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    task.name = name
    task.save()


def set_text_by_id(task_id, text):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    task.text = text
    task.save()

#+
def set_famaly_by_id(task_id, famaly):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    task.parent_id = famaly
    task.save()

#+
def set_stasus_by_id(task_id, status):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    task.status = status
    task.save()

#+
def set_priority_by_id(task_id, priority):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    task.priority = priority
    task.save()

#+
def set_createdate_by_id(task_id, createdate):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    task.create_date = createdate
    task.save()

#+
def set_deadline_by_id(task_id, deadline):
    task = TaskTable.select().where(TaskTable.id == task_id).get()
    task.deadline = deadline
    task.save()
################################
#++++
def set_person_by_id(task_id, person):
    table = PersonTable()
    table.persons_name = person
    table.task = TaskTable.select().where(TaskTable.id == task_id).get()
    table.save()

#++++
def set_admin_by_id(task_id, admin):
    table = AdminTable()
    table.admins_name = admin
    table.task = TaskTable.select().where(TaskTable.id == task_id).get()
    table.save()

##+++++
def remove_person_by_id(task_id, person):
    for persone in PersonTable.select().join(TaskTable).where(TaskTable.id == task_id):
        if persone.persons_name == person:
            person.delete_instance()


##++++
def remove_admin_by_id(task_id, admin):
    '''
    admin = AdminTable.get(AdminTable.task_id == task_id & AdminTable.admins_name == admin)
    admin.delete_instance()
    '''
    for admine in AdminTable.select().join(TaskTable).where(TaskTable.id == task_id):
        if admine.admins_name == admin:
            admine.delete_instance()

#+
def delete_task_by_id(task_id):
    task = TaskTable.get(TaskTable.id == task_id)
    task.delete_instance()

#---
def set_login_password(login, password):
    jsondata = from_file_by_path(take_users_path())
    jsondata.append({'login': login, 'password': password})
    save_by_path(jsondata, take_users_path())

#---
def check_log_pass(login, password):
    jsondata = from_file_by_path(take_users_path())
    for dic in jsondata:
        if dic["login"] == login and dic["password"] == password:
            return True
    raise exceptions.UserError('Неверные имя пользователя или пароль.')


##-----------
def set_login_person(login):
    jsondata = from_file_by_path(take_login_path())
    jsondata["logperson"] = login
    save_by_path(jsondata, take_login_path())

##-----------
def get_login_person():
    jsondata = from_file_by_path(take_login_path())
    if jsondata:
        return jsondata["logperson"]
    else:
        return "default"

##-----------
def get_persons():
    jsondata = from_file_by_path(take_users_path())
    out_list = list()
    for dic in jsondata:
        out_list.append(dic["login"])
    return out_list

##-----------
def exit_person_f():
    jsondata = from_file_by_path(take_login_path())
    jsondata["logperson"] = None
    save_by_path(jsondata, take_login_path())

#++
def set_recc_task(typetask, text, famaly, createdate, name, nextdate, priority, creater):
    rec_task = RecurrentTable()
    rec_task.name = name
    rec_task.text = text
    rec_task.typetask = typetask
    rec_task.parent_id = famaly
    rec_task.createdate = createdate
    rec_task.startdate = nextdate
    rec_task.priority = priority
    rec_task.creater = creater
    rec_task.save()
    #rec_task.creater =
    #logging.info("Recurrent task {0} was created by {1} ".format(rec_id, get_login_person()))



#+++
def remove_rec_task_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    rec_task.delete_instance()

##++
def get_rec_creater_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return rec_task.creater

##++
def get_rec_text_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return rec_task.text

#+
def get_rec():
    out_list = list()
    for rec_task in RecurrentTable.select():
        out_list.append(rec_task)
    return out_list


def get_rec_ids():
    out_list = list()
    for rec_task in RecurrentTable.select():
        out_list.append(rec_task.id)
    return out_list

#+
def get_rec_type_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return rec_task.typetask

#+
def get_rec_name_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return rec_task.name

#+
def get_rec_nextdate_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return int(datetime.strftime(rec_task.startdate, "%Y%m%d"))


def get_rec_nextdate_by_id_in_datetime(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return rec_task.startdate

#+
def get_rec_famaly_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return rec_task.parent_id

#+
def get_rec_priority_by_id(rec_id):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    return rec_task.priority

#+
def set_rec_nextdate_by_id(rec_id, nextdate):
    rec_task = RecurrentTable.select().where(RecurrentTable.id == rec_id).get()
    rec_task.startdate = nextdate
    rec_task.save()

##+
def get_ids_parents_by_id(task_id):
    out_list = list()
    task = TaskTable.get_or_none(TaskTable.id == task_id)
    if task:
        while task.parent_id:
            out_list.append(task.parent_id)
            task = TaskTable.get_or_none(TaskTable.id == task.parent_id)
    return out_list

##-----------+
def check_id(task_id):
    if task_id in get_tasks_ids():
        return True
    return False

##-----------+
def check_rec_id(rec_id):
    if rec_id in get_rec_ids():
        return True
    return False
