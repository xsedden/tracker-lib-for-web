import logging
import subprocess
from datetime import datetime

from main.lib.person import  check_access_admin, check_access_creater
from main.lib import exceptions
from main.lib.convert_data import get_tasks_ids, get_deadline_by_id, delete_task_by_id, \
    get_status_by_id,check_id, \
    set_stasus_by_id, get_ids_by_famaly, get_text_by_id, set_deadline_by_id, set_famaly_by_id, get_ids_parents_by_id, \
    set_name_by_id, set_priority_by_id, set_text_by_id


def finish_task(task_id, user):
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    ###check_login()
    if check_access_admin(task_id, user) or check_access_creater(task_id, user):
        status = get_status_by_id(task_id)
        if status != 1:
            status = 1
        else:
            status = 0
        set_stasus_by_id(task_id, status)
        _rec_finish_task(task_id)
    ###logging.info("task {0} was finished by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def _rec_finish_task(famaly):
    ids = get_ids_by_famaly(famaly)
    for task_id in ids:
        status = get_status_by_id(task_id)
        if status != 1:
            status = 1
        else:
            status = 0
        set_stasus_by_id(task_id, status)
        _rec_finish_task(task_id)


def delete_task(task_id, user):
    if isinstance(task_id, list):
        for task in task_id:
            """if not check_id(task):
                raise exceptions.IDError('Не коректный ID.')"""
            if check_access_creater(task, user):
                son = check_son(task)
                if son:
                    delete_task(son, user)
                delete_task_by_id(task)
            ###logging.info("task {0} was removed by {1}".format(task, get_login_person()))
            else:
                raise exceptions.UserError('Недостаточно прав пользователя.')

    else:
        """if not check_id(task_id) and not task_id == 0:
            raise exceptions.IDError('Не коректный ID.')"""
        if check_access_creater(task_id, user):
            son = check_son(task_id)
            if son:
                delete_task(son, user)
            delete_task_by_id(task_id)
            ###logging.info("task {0} was removed by {1}".format(task_id, get_login_person()))
        else:
            raise exceptions.UserError('Недостаточно прав пользователя.')


def check_son(task_id):
    return get_ids_by_famaly(task_id)

#--
def edit_text(text, task_id, user):
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')

    ##check_login()
    if check_access_admin(task_id, user) or check_access_creater(task_id, user):
        set_text_by_id(task_id, text)
    #if get_text_by_id(task_id):
    ###    subprocess.call(['vim', get_text_by_id(task_id)])
    ###    logging.info("text from task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
       raise exceptions.UserError('Недостаточно прав пользователя.')


def convert_date(date):
    return date.replace(".", "")[:8]


def check_deadline():
    now = datetime.strftime(datetime.now(), "%Y%m%d")
    for task_id in get_tasks_ids():
        if get_deadline_by_id(task_id) < int(now) and get_status_by_id(task_id) is 0:
            set_stasus_by_id(task_id, 2)
            ###logging.info("was deadline for task {}".format(task_id))


def edit_deadline(task_id, deadline, user):
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    ##check_login()
    if check_access_admin(task_id, user) or check_access_creater(task_id, user):
        set_deadline_by_id(task_id, deadline)
        ###logging.info("deadline for task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def edit_name(task_id, name, user):
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    ###check_login()
    if check_access_admin(task_id, user) or check_access_creater(task_id, user):
        set_name_by_id(task_id, name)
        ###logging.info("name for task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def edit_priority(task_id, priority, user):
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    ###check_login()
    if check_access_admin(task_id, user) or check_access_creater(task_id, user):
        set_priority_by_id(task_id, priority)
        ###logging.info("priority for task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')



def edit_famaly(task_id, famaly, user):
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    ###check_login()
    if check_access_admin(task_id, user) or check_access_creater(task_id, user):
        if task_id in get_ids_parents_by_id(famaly):
            raise exceptions.TaskError('Узбагойся. Так делать не н-н-н-ннада=)')
        else:
            set_famaly_by_id(task_id, famaly)
            ###logging.info("famaly for {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')

