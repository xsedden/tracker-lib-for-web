FROM ubuntu
FROM python

WORKDIR /app

COPY . .

RUN python setup.py install

CMD ["bash"]
